
from flask import Flask
import time
import math
from flask import request
import sys

def find_near_prime(i):
    while True:
        i += 1

        if (i > 10) and ( (i % 2 == 0) or (i % 10 == 0) ):
            continue

        for j in range(2, int(math.sqrt(i) + 1) ):
            if i % j == 0:
                break
        else:
            return i

app = Flask(__name__)


@app.route('/nearestPrime')
def hello_world():

    number = request.args.get('number')
    show_time = request.args.get('time')

    if number is None:
        return 'GET args {number} not set!'

    try:
        number = int(number)
    except ValueError:
        return 'GET args {number} must be integer!'
    
    if number:
        
        if show_time:
            start = time.time()
            return "%s / time %s" % (find_near_prime(number), time.time() - start)
        else:
            return str(find_near_prime(number))
        

    

if __name__ == "__main__":
    try:
        port = int(sys.argv[1])
        app.run(host='0.0.0.0', port=port)
    except (IndexError, ValueError):
        print('Port args must be set!')
        
    



