

# Установка 

```
pip install -r  requirements.txt
```

# Запуск 

```
python main.py 5000
```

# Получение простого числа

```
/nearestPrime?number=1000000
```

# Вывод времени поиска простого числа в секундах

```
/nearestPrime?number=1000000&time=Y
```